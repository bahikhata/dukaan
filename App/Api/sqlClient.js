
/*const uuid = require("uuid/v4");
var SQLite = require("react-native-sqlite-storage");
SQLite.DEBUG(false);
SQLite.enablePromise(true);
var DB_NAME= "test.db";
var DB_VERSION = "1.0";
var DB_DISPLAYNAME = "Test Database";
var DB_Size = 200000;

function openCB() {
  //console.log("Database Opened");
}

function errorCB() {
  //console.log("Error");
}

function initDB(){
  let db;
  return new Promise((resolve) => {
    SQLite.echoTest()
      .then(() => {
        SQLite.openDatabase(
          DB_NAME,
          DB_VERSION,
          DB_DISPLAYNAME,
          DB_Size
        ).then(DB => {
            db = DB;
            db.executeSql('SELECT 1 FROM users LIMIT 1').then(() => {
                //console.log("Database is ready ... executing query ...");
            }).catch((error) =>{
                console.log("Received error: ", error);
                populateDB();
            });
            resolve(db);
          })
          .catch(error => {
            errorCB(error);
          });
      })
      .catch(error => {
        console.log("echoTest failed - plugin not functional");
      });
    });
}

function closeDB(db){
  if (db) {
    db.close()
      .then(status => {
        //console.log("Database CLOSED");
      })
      .catch(error => {
        errorCB(error);
      });
  } else {
    console.log("Database was not OPENED");
  }
}

export function populateDB(){
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      db.transaction(tx => {
        //tx.executeSql("DROP TABLE `users`");
        //tx.executeSql("DROP TABLE `accounts`");
        //tx.executeSql("DROP TABLE `transactions`");
        //tx.executeSql("DROP TABLE `banks`")
        // `accounts` `banks` `transactions`");
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS `users` (`id` UUID NOT NULL PRIMARY KEY, `name` VARCHAR(255), `period` DATETIME, `phone` VARCHAR(15) NOT NULL, `syncState` INTEGER DEFAULT 1, `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, `updatedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP)"
        );
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS `accounts` (`id` UUID NOT NULL PRIMARY KEY, `name` VARCHAR(255), `phone` VARCHAR(255), `amount` DOUBLE PRECISION DEFAULT 0, `cancelled` BOOLEAN DEFAULT 0, `syncState` INTEGER DEFAULT 1, `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, `updatedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, `userId` UUID REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE)"
        );
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS `banks` (`id` UUID NOT NULL PRIMARY KEY, `name` VARCHAR(255), `detail` JSON, `amount` DOUBLE PRECISION, `syncState` INTEGER DEFAULT 1, `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, `updatedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, `userId` UUID REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE)"
        );
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS `transactions` (`id` UUID NOT NULL PRIMARY KEY, `amount` INTEGER, `detail` TEXT, `media` VARCHAR(255), `cancelled` BOOLEAN DEFAULT 0, `syncState` INTEGER DEFAULT 1, `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, `updatedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, `userId` UUID REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE, `accountId` UUID REFERENCES `accounts` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,`bankId` UUID REFERENCES `banks` (`id`) ON DELETE SET NULL ON UPDATE CASCADE)"
        );
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS `stocks` (`id` UUID NOT NULL PRIMARY KEY, `name` VARCHAR(255), `quantity` DOUBLE PRECISION DEFAULT 0, `cancelled` BOOLEAN DEFAULT 0, `syncState` INTEGER DEFAULT 1, `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, `updatedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, `userId` UUID REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE)"
        );
      }).then(() => {
        console.log("Tables created");
        resolve(true);
      }).catch(err => {
        console.log(err);
        resolve(false);
      });
    }).catch(err => {
      console.log(err);
    });
  });
}

export function checkIfTablesExist(){
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      db.transaction(tx => {
        var sql = "SELECT name FROM sqlite_master WHERE name=?"; // AND name='users'";
        tx.executeSql(sql, ['users'], (tx, results) => {
          if(results.rows.length > 0){
            resolve(true);
          }
          resolve(false);
        }).then(result => {
          closeDB(db);
        }).catch(err => {
          console.log(err);
        });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function stock(name, quantity, userId) {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      db.transaction(tx => {
        var sql = "INSERT INTO stocks(id, name, quantity, userId) VALUES(?,?,?,?)";
        tx.executeSql(sql, [uuid(), name, quantity, userId], (tx, results) => {
          resolve(true);
        }).then(result => {
          closeDB(db);
        }).catch(err => {
          console.log(err);
        });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function getStocks() {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      const accounts = [];
      db.transaction(tx => {
        var sql = "SELECT * FROM stocks";
        tx.executeSql(sql, [], (tx, results) => {
            let len = results.rows.length;
            for(let i=0; i<len; i++){
              let row = results.rows.item(i);
              accounts.push(row);
            }
            resolve(accounts);
          });
      }).then(result => {
        closeDB(db);
      }).catch(err => {
        console.log(err);
      });
    }).catch(err => {
      console.log(err);
    });
  });
}

export function account(name, phone, userId) {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      db.transaction(tx => {
        var sql = "INSERT INTO accounts(id, name, phone, cancelled, userId) VALUES(?,?,?,?,?)";
        tx.executeSql(sql, [uuid(), name, phone, false, userId], (tx, results) => {
          resolve(true);
        }).then(result => {
          closeDB(db);
        }).catch(err => {
          console.log(err);
        });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function getAccounts() {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      const accounts = [];
      db.transaction(tx => {
        var sql = "SELECT * FROM accounts ORDER BY cancelled, name";
        tx.executeSql(sql, [], (tx, results) => {
            let len = results.rows.length;
            for(let i=0; i<len; i++){
              let row = results.rows.item(i);
              accounts.push(row);
            }
            resolve(accounts);
          });
      }).then(result => {
        closeDB(db);
      }).catch(err => {
        console.log(err);
      });
    }).catch(err => {
      console.log(err);
    });
  });
}

export function getAccountByName(name) {
  return new Promise((resolve, reject) => {
    initDB().then((db) => {
      db.transaction((tx) => {
        var sql = "SELECT * FROM accounts WHERE name=? LIMIT 1";
        tx.executeSql(sql, [name],(tx, results) => {
            if(results.rows.length > 0){
                resolve(results.rows.item(0));
            } else {
                resolve(null);
            }
          }).then((result) => {
              closeDB(db);
          }).catch((err) => {
            console.log(err);
          });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function getAccountById(id) {
  return new Promise((resolve, reject) => {
    initDB().then((db) => {
      db.transaction((tx) => {
        var sql = "SELECT * FROM accounts WHERE id=? LIMIT 1";
        tx.executeSql(sql, [id],(tx, results) => {
            if(results.rows.length > 0){
                resolve(results.rows.item(0));
            } else {
                resolve(null);
            }
          }).then((result) => {
              closeDB(db);
          }).catch((err) => {
            console.log(err);
          });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function fetchAccountElseCreate(_account, userId) {
  return new Promise((resolve, reject) => {
      //Check if account exists.
      this.getAccountByName(_account).then(account => {
        if(account != null){
          resolve(account);
        } else {
          //If account doesn't exist then create a new account.
          this.account(_account,"", userId).then(result => {
            if(result){
              this.getAccountByName(_account).then(newAccount => {
                resolve(newAccount);
              });
            }
          });
        }
      });
  });
}

export function updateAccountAmount(accountId,amount) {
  return new Promise((resolve, reject) => {
    initDB().then((db) => {
      db.transaction((tx) => {
        var sql = "UPDATE accounts SET amount = amount + ?, updatedAt = ? WHERE id = ?";
        tx.executeSql(sql, [amount, new Date().toISOString(), accountId],(tx, results) => {
            if(results.rowsAffected > 0){
              resolve(true);
            }
            resolve(null);
          }).then((result) => {
              closeDB(db);
          }).catch((err) => {
            console.log(err);
          });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function updateAccount(id, _account) {
  return new Promise((resolve, reject) => {
    initDB().then((db) => {
      db.transaction((tx) => {
        var sql = "UPDATE accounts SET name = ?, phone = ?, updatedAt = ? WHERE id = ?";
        tx.executeSql(sql, [_account.name, _account.phone, new Date().toISOString(), id],(tx, results) => {
            if(results.rowsAffected > 0){
                resolve(results.rows);
            } else {
                resolve(false);
            }
          }).then((result) => {
              closeDB(db);
          }).catch((err) => {
            console.log(err);
          });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function closeAccount(id) {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      db.transaction(tx => {
        var sql = "UPDATE accounts SET cancelled = ?, updatedAt = ? WHERE id=?";
        tx.executeSql(sql, [true, new Date().toISOString(), id], (tx, results) => {
          if(results.rowsAffected > 0){
            resolve(true);
          } else {
            resolve(false);
          }
        }).then(result => {
          closeDB(db);
        }).catch(err => {
          console.log(err);
        });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function reOpenAccount(id) {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      db.transaction(tx => {
        var sql = "UPDATE accounts SET cancelled = ?, updatedAt = ? WHERE id=?";
        tx.executeSql(sql, [false, new Date().toISOString(), id], (tx, results) => {
          if(results.rowsAffected > 0){
            resolve(true);
          } else {
            resolve(false);
          }
        }).then(result => {
          closeDB(db);
        }).catch(err => {
          console.log(err);
        });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function user(name, period, phone) {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      db.transaction(tx => {
        var sql = "INSERT INTO users(id, name, period, phone) VALUES(?,?,?,?)";
        var timestamp = new Date();
        tx.executeSql(sql, [uuid(), name, period, phone], (tx, results) => {
          resolve(true);
        }).then(result => {
          closeDB(db);
        }).catch(err => {
          console.log(err);
        });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function getUser() {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      db.transaction(tx => {
        var sql = "SELECT * FROM users LIMIT 1";
        tx.executeSql(sql, [], (tx, results) => {
          results.rows.length > 0 ? resolve(results.rows.item(0)) : resolve(null);
        }).then(result => {
          closeDB(db);
        }).catch(err => {
          console.log(err);
          resolve(null);
        });
      }).catch(err => {
        console.log(err);
        resolve(null);
      });
    });
  });
}

export function getUsername() {
  return new Promise((resolve, reject) => {
    initDB().then((db) => {
      db.transaction((tx) => {
        var sql = "SELECT name FROM users";
        tx.executeSql(sql, [],(tx, results) => {
            if(results.rows.length > 0){
                resolve(results.rows.item(0).name);
            } else {
                resolve(null);
            }
          }).then((result) => {
              closeDB(db);
          }).catch((err) => {
            console.log(err);
          });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function updateUser(id, name, period) {
  return new Promise((resolve, reject) => {
    initDB().then((db) => {
      db.transaction((tx) => {
        var sql = "UPDATE users SET name = ?, period = ?, updatedAt = ? WHERE id = ?";
        tx.executeSql(sql, [name, period, new Date().toISOString(), id],(tx, results) => {
            resolve(true);
          }).then((result) => {
              closeDB(db);
          }).catch((err) => {
            console.log(err);
          });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function updateUserPhone(id, phone) {
  return new Promise((resolve, reject) => {
    initDB().then((db) => {
      db.transaction((tx) => {
        var sql = "UPDATE users SET phone = ?, updatedAt = ? WHERE id = ?";
        tx.executeSql(sql, [phone, new Date().toISOString(), id],(tx, results) => {
            console.log(results.rows);
            if(results.rows.length > 0){
                resolve(results.rows);
            } else {
                resolve(null);
            }
          }).then((result) => {
              closeDB(db);
          }).catch((err) => {
            console.log(err);
          });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function bank() {
  return ["SBI", "PNB"];
}

export function transaction(accountName, amount, detail, attachment, date, multiplier, userId) {
  return new Promise((resolve, reject) => {
    if(accountName == ""){
      initDB().then(db => {
        db.transaction(tx => {
          var sql = "INSERT INTO transactions(id, accountId, amount, detail, cancelled, createdAt, userId) VALUES(?,?,?,?,?,?,?)";
          tx.executeSql(sql, [uuid(), null, amount * multiplier, detail, false, date, userId],(tx, results) => {

                if(results.rowsAffected > 0){
                  resolve(true);
                } else {
                  resolve(null);
                }
            }).then((result) => {
                closeDB(db);
            }).catch((err) => {
              console.log(err);
            });
        }).catch(err => {
          console.log(err);
        });
      });
    } else {
      //Fetch Account Else create new account
      this.fetchAccountElseCreate(accountName, userId).then(account => {
        initDB().then(db => {
          db.transaction(tx => {
            var sql = "INSERT INTO transactions(id, accountId, amount, detail, cancelled, createdAt, userId) VALUES(?,?,?,?,?,?,?)";
            tx.executeSql(sql, [uuid(), account.id, amount * multiplier, detail, false, date, userId],(tx, results) => {
                if(results.rowsAffected > 0){
                    this.updateAccountAmount(account.id, amount * multiplier).then(result => {
                      console.log(result);
                    })
                    resolve(true);
                } else {
                    resolve(null);
                }
              }).then((result) => {
                  closeDB(db);
              }).catch((err) => {
                console.log(err);
              });
          }).catch(err => {
            console.log(err);
          });
        });
      });
    }
  });
}

export function getTxns(firstDate, nextDate) {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      const txns = [];
      db.transaction(tx => {
        var sql = "SELECT name, accountId, transactions.id, transactions.amount, detail, media, transactions.cancelled, transactions.userId FROM transactions LEFT JOIN accounts on accounts.id = transactions.accountId WHERE transactions.createdAt >= ? AND transactions.createdAt < ?";
        tx.executeSql(sql, [firstDate, nextDate], (tx, results) => {
            let len = results.rows.length;
            for(let i=0; i<len; i++){
              let row = results.rows.item(i);
              //console.log(row);
              txns.push(row);
            }
            resolve(txns);
          });
      }).then(result => {
        closeDB(db);
      }).catch(err => {
        console.log(err);
      });
    }).catch(err => {
      console.log(err);
    });
  });
}

export function getTxnById(id) {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      const txns = [];
      db.transaction(tx => {
        var sql = "SELECT * FROM transactions WHERE id=? LIMIT 1";
        tx.executeSql(sql, [id], (tx, results) => {
            let txn = results.rows.item(0);
            resolve(txn);
          });
      }).then(result => {
        closeDB(db);
      }).catch(err => {
        console.log(err);
      });
    }).catch(err => {
      console.log(err);
    });
  });
}

export function getTxnsByAccountId(id) {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      const txns = [];
      db.transaction(tx => {
        var sql = "SELECT * FROM transactions WHERE accountId=?" ;
        tx.executeSql(sql, [id], (tx, results) => {
            let len = results.rows.length;
            for(let i=0; i<len; i++){
              let row = results.rows.item(i);
              txns.push(row);
            }
            resolve(txns);
          });
      }).then(result => {
        closeDB(db);
      }).catch(err => {
        console.log(err);
      });
    }).catch(err => {
      console.log(err);
    });
  });
}

export function deleteTransaction(txnId) {
  return new Promise((resolve, reject) => {
    getTxnById(txnId).then(txn => {
      if(txn){
        let accountId = txn.accountId;
        let amount = txn.amount;
        initDB().then((db) => {
          db.transaction((tx) => {
            var sql = "UPDATE transactions SET cancelled = ?, updatedAt = ? WHERE id=?";
            if (accountId != null) {
              var sql2 = "UPDATE accounts SET amount = amount - ? WHERE id = ?";
              tx.executeSql(sql2, [amount, accountId]);
            }
            tx.executeSql(sql, [true, new Date().toISOString(), txn.id],(tx, results) => {
              if(results.rowsAffected > 0){
                resolve(true);
              } else {
                resolve(false);
              }
            }).then((result) => {
                closeDB(db);
            }).catch((err) => {
              console.log(err);
            });
          }).catch(err => {
            console.log(err);
          });
        });
      }
    });
  });
  /*return new Promise((resolve, reject) => {
    initDB().then(db => {
      db.transaction(tx => {
        getTxnById(txnId).then(txn => {
          if(txn){
            let accountId = txn.accountId;
            let amount = txn.amount;
            if (accountId != null) {
              console.log(txn.id);
              var sql = "UPDATE transactions SET cancelled = ?,updatedAt = ? WHERE id=?";
              //var sql2 = "UPDATE accounts SET amount = amount - ? WHERE id = ?";
              //tx.executeSql(sql2, [amount, accountId]);
              tx.executeSql(sql, [true, new Date().toISOString(), txn.id], (tx, results) => {
                if(results.rowsAffected > 0){
                  resolve(true);
                } else {
                  resolve(false);
                }
              });
            }
          }
        });
      }).then(result => {
        closeDB(db);
      }).catch(err => {
        console.log(err);
      });
    }).catch(err => {
      console.log(err);
    });
  });*/
/* }

export function updateTransaction(id, detail) {
  return new Promise((resolve, reject) => {
    initDB().then((db) => {
      db.transaction((tx) => {
        var sql = "UPDATE transactions SET detail = ?, updatedAt = ? WHERE id = ?";
        var updatedDate = new Date();
        tx.executeSql(sql, [detail, new Date().toISOString(), id],(tx, results) => {
            if(results.rows.length > 0){
                resolve(results.rows);
            } else {
                resolve(null);
            }
          }).then((result) => {
              closeDB(db);
          }).catch((err) => {
            console.log(err);
          });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function sumTillDate(date) {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      db.transaction(tx => {
        const txns = [];
        var sql = "SELECT SUM(amount) as sum FROM transactions WHERE createdAt <= ? AND cancelled != 1";
        tx.executeSql(sql, [date], (tx, results) => {
          results.rows.length > 0 ? resolve(results.rows.item(0)) : reject(null);
        });
      }).then(result => {
        closeDB(db);
      }).catch(err => {
        console.log(err);
      });
    }).catch(err => {
      console.log(err);
    });
  });
}

export function deleteAll() {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      db.transaction(tx => {
        //tx.executeSql("TRUNCATE TABLE users");
        tx.executeSql("TRUNCATE TABLE transactions");
        //tx.executeSql("TRUNCATE TABLE accounts");
        //tx.executeSql("TRUNCATE TABLE banks");
      }).then(result => {
        resolve("Emptied !!");
        closeDB(db);
      }).catch(err => {
        console.log(err);
      });
    }).catch(err => {
      console.log(err);
    });
  });
}

export function test(amount, detail, attachment, date) {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      db.transaction(tx => {
        var sql = "INSERT INTO transactions(id, amount, detail, createdAt) VALUES(?,?,?,?)";
        tx.executeSql(sql, [uuid(), amount, detail, date],(tx, results) => {
              if(results.rowsAffected > 0){
                resolve(true);
              } else {
                resolve(null);
              }
          }).then((result) => {
              closeDB(db);
          }).catch((err) => {
            console.log(err);
          });
      }).catch(err => {
        console.log(err);
      });
    });
  });
}

export function getTest() {
  return new Promise((resolve, reject) => {
    initDB().then(db => {
      const txns = [];
      db.transaction(tx => {
        var sql = "SELECT * FROM transactions";
        tx.executeSql(sql, [], (tx, results) => {
            let len = results.rows.length;
            for(let i=0; i<len; i++){
              let row = results.rows.item(i);
              //console.log(row);
              txns.push(row);
            }
            resolve(txns);
          });
      }).then(result => {
        closeDB(db);
      }).catch(err => {
        console.log(err);
      });
    }).catch(err => {
      console.log(err);
    });
  });
}

export function sync(){

}

export function send(amount, otp){
  fetch('https://reqres.in/api/transfer', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      amount: amount,
      otp: otp
    })
  }).then(response => response.json())
  .then(responseJson => console.log(responseJson));
}

export function receive(amount, otp){
  return new Promise((resolve, reject) => {
    fetch('https://reqres.in/api/transfer', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        amount: amount,
        otp: otp
      })
    }).then(response => response.json())
    .then(responseJson => {
      resolve(responseJson.id);
    });
  });
}
*/
