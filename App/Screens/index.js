import { Navigation } from "react-native-navigation";

import Home from "./Home";
import Orders from "./Orders";
import Categories from "./Categories";
import Products from "./Products";
import Account from "./Account";

export function registerScreens() {
  Navigation.registerComponent("home", () => Home);
  Navigation.registerComponent("orders", () => Orders);
  Navigation.registerComponent("categories", () => Categories);
  Navigation.registerComponent("products", () => Products);
  Navigation.registerComponent("account", () => Account);
}
