import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, Text, View, Image,
  DatePickerAndroid, Alert,
  TouchableHighlight, FlatList, Button,
  ImageBackground, PermissionsAndroid
} from 'react-native';
//import Icon from 'react-native-vector-icons/FontAwesome';
import { iconsMap, iconsLoaded } from '../app-icons';
import { Colors, Metrics, Images, ApplicationStyles, Fonts } from '../Themes';

const I18n = require('../I18n');
import { Navigation } from 'react-native-navigation';

export default class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };

    iconsLoaded.then(() => {
        //this.initialMenu();
    });

    this.screenState = null;

    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount() {

  }

  componentWillUnmount() {

  }

  componentDidAppear(){

  }

  componentDidDisappear(){

  }

  render() {
    const {  } = this.state;
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };
    return (
      <>
        <Text>Products</Text>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    resizeMode: 'cover'
  },
  imageBackground: {
    width: '100%',
    flex: 1,
    flexDirection: 'row'
  },
  openAmount: {
    flex: 0.27,
    fontFamily: 'Hind-Regular',
    color: Colors.charcoal,
    fontSize: Fonts.size.small,
    fontStyle: 'italic',
    textAlignVertical: 'center',
    textAlign: 'right'
  },
  openDetail: {
    flex: 0.73,
    color: Colors.hairline,
    fontFamily: 'Hind-Regular',
    fontSize: Fonts.size.medium,
    textAlignVertical: 'center',
    fontWeight: 'bold',
    marginLeft: Metrics.smallMargin
  },
  group: {
    flexDirection: 'row',
    marginTop: Metrics.baseMargin / 2,
    marginBottom: Metrics.baseMargin / 2
  },
  barsMenu: {
    flex: 0.1,
    textAlignVertical: 'center',
    margin: Metrics.doubleBaseMargin - 2
  },
  calendarMenu: {
    flex: 0.07,
    textAlignVertical: 'center'
  },
  date: {
    flex: 0.25,
    fontSize: Fonts.size.h4,
    color: Colors.primary,
    justifyContent: 'center',
    textAlign: 'center',
    alignSelf: 'center',
    fontStyle: 'italic',
    textAlignVertical: 'center'
  },
  dateContainer: {
    flex: 0.1,
    width: '100%',
    backgroundColor: Colors.peach,
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center'
  },
  listHeader: {
    flex: 0.05,
    flexDirection: 'row',
    backgroundColor: Colors.peach,
    padding: Metrics.baseMargin/2,
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.darkPrimary
  },
  listIcon: {
    flex: 0.4,
    textAlign: 'right',
    paddingRight: Metrics.baseMargin
  },
  listHeaderTitle: {
    flex: 0.6,
    color: Colors.darkPrimary,
    fontSize: 14,
    fontStyle: 'italic'
  }
});
