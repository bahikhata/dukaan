import { Navigation } from "react-native-navigation";
import { registerScreens } from "./Screens";
import { home, register, playground, options, dummy } from "./Layouts";
import "reflect-metadata";
import { Colors } from "./Themes";
//import { api } from "./Api";

registerScreens();

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setDefaultOptions(options);

  /*api.getUser().then(result => {
    if(result !== null){
      Navigation.setRoot({
        root: playground
      });
    } else {
      Navigation.setRoot({
        root: register
      });
    }
  });*/
  Navigation.setRoot({
    root: home
  });
});
