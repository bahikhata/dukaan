const I18n = require('../I18n');

import { Colors, Fonts } from '../Themes';

export const home = {
  bottomTabs: {
    id: 'tabs',
    children: [
      {
        stack: {
          children: [{
            component: {
              name: "home",
              options: {
                topBar: {
                  visible: true,
                  title: {
                    text: I18n.t('home')
                  }
                },
                bottomTab: {
                  text: I18n.t('home'),
                  icon: require('../Images/swastika.png'),
                  iconColor: Colors.black,
                  selectedIconColor: Colors.charcoal
                }
              }
            }
          }]
        }
      },
      {
        stack: {
          children: [
            {
              component: {
                name: "products",
                options: {
                  bottomTab: {
                    text: I18n.t("products"),
                    icon: require('../Images/swastika.png'),
                    iconColor: Colors.black,
                    selectedIconColor: Colors.charcoal
                  },
                  topBar: {
                    title: {
                      text: I18n.t('products')
                    }
                  }
                }
              }
            }
          ]
        }
      },
      {
        stack: {
          children: [
            {
              component: {
                name: "categories",
                options: {
                  bottomTab: {
                    text: I18n.t("categories"),
                    icon: require('../Images/swastika.png'),
                    iconColor: Colors.black,
                    selectedIconColor: Colors.charcoal
                  },
                  topBar: {
                    title: {
                      text: I18n.t('categories')
                    }
                  }
                }
              }
            }
          ]
        }
      },
      {
        stack: {
          children: [
            {
              component: {
                name: "account",
                options: {
                  bottomTab: {
                    text: I18n.t("account"),
                    icon: require('../Images/swastika.png'),
                    iconColor: Colors.black,
                    selectedIconColor: Colors.charcoal
                  },
                  topBar: {
                    title: {
                      text: I18n.t('account')
                    }
                  }
                }
              }
            }
          ]
        }
      }
    ]
  }
};
